
RELEASENAME = "Hoopy McGee - Project Sunflower %y%m%d"

# you can modify these options
CURRENTEPUB = current.epub
SOURCE      = ./src/
EPUBFILE    = ./build/ebook.epub
SUBSETEPUB  = ./build/ebook-subsetted.epub
WEIGHTEPUB  = ./build/ebook-weighted.epub
KEPUBFILE   = ./build/ebook.kepub.epub
KINDLEFILE  = ./build/ebook.mobi
PDFFILE     = ./build/book.pdf


SHELL := /bin/bash
SOURCEFILES = $(shell find $(SOURCE) | sort)
XHTMLFILES = $(shell find $(SOURCE) -name '*.xhtml' | sort)
TEXPARTS = $(shell find $(SOURCE) -name '*.xhtml' | grep -v 'nav\.xhtml\|cover\.xhtml\|title\.xhtml' | sed -e 's/.*Text\/\(.*\)\.xhtml/.\/build\/latex\/\1.xhtml.tex/' | sort)
PDFPARTS = $(shell find $(SOURCE) -name '*.xhtml' | grep -v 'nav\.xhtml\|cover\.xhtml\|title\.xhtml' | sed -e 's/.*Text\/\(.*\)\.xhtml/.\/build\/parts\/\1.pdf/' | sort)
PNGFILES = $(shell find $(SOURCE) -name '*.png' | sort)

EPUBCHECK = ./tools/epubcheck/epubcheck.jar
KINDLEGEN = ./tools/kindlegen/kindlegen

EBOOKPOLISH := $(shell command -v ebook-polish 2>&1)
EBOOKVIEWER := $(shell command -v ebook-viewer 2>&1)
JAVA        := $(shell command -v java 2>&1)
INOTIFYWAIT := $(shell command -v inotifywait 2>&1)

EPUBCHECK_VERSION = 4.2.2
# https://github.com/IDPF/epubcheck/releases
EPUBCHECK_URL = https://github.com/IDPF/epubcheck/releases/download/v$(EPUBCHECK_VERSION)/epubcheck-$(EPUBCHECK_VERSION).zip
# http://www.amazon.com/gp/feature.html?docId=1000765211
KINDLEGEN_URL = http://kindlegen.s3.amazonaws.com/kindlegen_linux_2.6_i386_v2_9.tar.gz

.PRECIOUS: build/latex/%.xhtml.tex
.PHONY: all clean build subset fontweight validate buildkepub buildkindle buildbook buildtexparts buildpdfparts buildcover extractcurrent watchcurrent release publish
all: build
release: clean build validate buildkepub buildkindle

build: $(EPUBFILE) $(SUBSETEPUB) $(WEIGHTEPUB)

$(EPUBFILE): $(SOURCEFILES)
	@echo "Building EPUB..."
	@mkdir -p `dirname $(EPUBFILE)`
	@rm -f "$(EPUBFILE)"
	@cd "$(SOURCE)" && zip -Xr9D "../$(EPUBFILE)" mimetype
	@cd "$(SOURCE)" && zip -Xr9D "../$(EPUBFILE)" . -x mimetype -x "*.DS_Store"

subset: $(SUBSETEPUB)
$(SUBSETEPUB): $(EPUBFILE)
ifndef EBOOKPOLISH
	@echo "Error: Calibre was not found. Unable to subset fonts."
	@exit 1
else
	@echo "Subsetting fonts. This may take a while..."
	@ebook-polish --verbose --subset-fonts "$(EPUBFILE)" "$(SUBSETEPUB)"
endif

fontweight: $(WEIGHTEPUB)
$(WEIGHTEPUB): $(SUBSETEPUB)
	@echo "Increasing weight of fonts..."
	@rm -rf build/fontweight
	@mkdir -p build/fontweight
	unzip "$(SUBSETEPUB)" 'OEBPS/Fonts/*' -d build/fontweight
	fontforge -script tools/KoboifyFonts.py \
		-r build/fontweight/OEBPS/Fonts/Quando-Regular.otf \
		-d build/fontweight -m -p -w 5 "Quando"
	@rm build/fontweight/*.sfd
	fontforge -script tools/KoboifyFonts.py \
		-r build/fontweight/OEBPS/Fonts/AdobeGaramondPro-Regular.otf \
		-i build/fontweight/OEBPS/Fonts/AdobeGaramondPro-Italic.otf \
		-s build/fontweight/OEBPS/Fonts/AdobeGaramondPro-Semibold.otf \
		-S build/fontweight/OEBPS/Fonts/AdobeGaramondPro-SemiboldItalic.otf \
		-d build/fontweight -m -p -w 8 "AdobeGaramondPro"
	@rm build/fontweight/*.sfd
	fontforge -script tools/KoboifyFonts.py \
		-r build/fontweight/OEBPS/Fonts/AdobeGaramondProSC-Regular.otf \
		-d build/fontweight -p -w 8 "AdobeGaramondProSC"
	@rm build/fontweight/*.sfd
	@cp build/fontweight/*.otf build/fontweight/OEBPS/Fonts/
	@cp "$(SUBSETEPUB)" "$(WEIGHTEPUB)"
	@cd "build/fontweight" && zip -Xr9D "../../$(WEIGHTEPUB)" OEBPS/Fonts/ -x "*.DS_Store"
	@rm -rf "build/fontweight"


buildkepub: $(KEPUBFILE)
$(KEPUBFILE): $(WEIGHTEPUB)
	@echo "Building Kobo EPUB..."
	@cp -f "$(WEIGHTEPUB)" "$(KEPUBFILE)"
	@for current in $(XHTMLFILES); do \
		mkdir -p "$$(dirname "tmpkepub/$$current")"; \
		echo "Kepubifying $$current..."; \
		./tools/kepubify.py "$$current" > "tmpkepub/$$current"; \
	done
	@cd "tmpkepub/$(SOURCE)" && zip -Xr9D "../../$(KEPUBFILE)" . -x "*.DS_Store"
	@rm -rf "tmpkepub/"

buildkindle: $(KINDLEFILE)
$(KINDLEFILE): $(WEIGHTEPUB) $(KINDLEGEN)
	@echo Building Kindle file...
	@rm -rf tmpkindle "$(KINDLEFILE).epub"
	@unzip "$(WEIGHTEPUB)" -d tmpkindle
	@for current in $$(find ./tmpkindle -name '*.otf' | sort); do \
		echo "Converting font $$current to TTF..."; \
		tools/ConvertFont.pe "$$current"; \
		rm "$$current"; \
	done
	@sed -i -e "s/\\.otf/.ttf/g; s/application\\/vnd.ms-opentype/font\\/ttf/g; s/scripted//g; s/ properties=\"\"//g" "tmpkindle/OEBPS/content.opf"
	@sed -i -e "s/\\.otf/.ttf/g; /max-width/d; /max-height/d; /-webkit-/d" "tmpkindle/OEBPS/Styles/style.css"
	@for current in $$(find ./tmpkindle -name '*.xhtml' | sort); do \
		sed -i -e "/<script/d; s/max-width:[^;\"]*;\\?//g" "$$current"; \
	done
	@for current in $$(find ./tmpkindle -name '*.png' | sort); do \
		channels=$$(identify -format '%[channels]' "$$current"); \
		if [[ "$$channels" == "graya" ]] || [[ "$$channels" == "rgba" ]]; then \
			echo "Converting $$current to RGB..."; \
			mogrify "$$current" -colorspace rgb -background white -flatten; \
		fi; \
	done
	@cd "tmpkindle" && zip -Xr9D "../$(KINDLEFILE).epub" mimetype
	@cd "tmpkindle" && zip -Xr9D "../$(KINDLEFILE).epub" . -x mimetype -x "*.DS_Store"
	@rm -rf "tmpkindle/"
	@$(KINDLEGEN) "$(KINDLEFILE).epub" -dont_append_source -c1 || exit 0 # -c1 means standard PalmDOC compression. -c2 takes too long but probably makes it even smaller.
	@#rm -f "$(KINDLEFILE).epub"
	@mv "$(KINDLEFILE).mobi" "$(KINDLEFILE)"


# Builds the LaTeX files from XHTML
build/latex/%.xhtml.tex: src/OEBPS/Text/%.xhtml src/OEBPS/Styles/style.css book/html2latex-config.py
	@mkdir -p "build/latex/"
	@book/html2latex-config.py --style "src/OEBPS/Styles/style.css" --input "$<" --output "$@" 2> >(grep -vw "WARNING	Property: Unknown Property name.")

buildtexparts: $(TEXPARTS)

build/tex/impnattypo/impnattypo.sty: tools/impnattypo/impnattypo.ins tools/impnattypo/impnattypo.dtx
	@rm -rf build/tex/impnattypo
	@mkdir -p build/tex/impnattypo
	@cd tools/impnattypo && latex -draftmode -output-directory=../../build/tex/impnattypo impnattypo.ins

# Builds the PDF from LaTeX files
buildbook: $(PDFFILE)
$(PDFFILE): $(TEXPARTS) book/* build/tex/impnattypo/impnattypo.sty tools/novel/tex/*
	@echo Building book...
	@tools/novelrun book/book.tex -O build/.book -o $(PDFFILE)
	@touch $(PDFFILE)

buildpdfparts: $(PDFPARTS)
build/parts/%.pdf: build/latex/%.xhtml.tex book/* build/tex/impnattypo/impnattypo.sty tools/novel/tex/*
	@echo Building part $*.pdf...
	@mkdir -p build/parts
	@tools/novelrun book/single.tex --latex-args="--jobname=\"$*\"" -O "build/.parts/$*" -o "$@"
	@touch "$@"



buildcover: build/cover.pdf
build/cover.pdf: artwork/cover.png book/cover.tex
	@echo Making book cover...
	@cd tools/novel/scripts && ./makecmyk ../../../../artwork/cover.png
	@tools/novelrun ./book/cover.tex -O build/.bookcover -o build/cover.pdf
	@mv tools/novel/scripts/output/cover-softproof.tif tools/novel/scripts/output/cover-NOTpdfx.pdf build/

$(EPUBCHECK):
	@echo Downloading epubcheck...
	@curl -o "epubcheck.zip" -L "$(EPUBCHECK_URL)" --connect-timeout 30
	@mkdir -p `dirname $(EPUBCHECK)`
	@unzip -q "epubcheck.zip"
	@rm -rf `dirname $(EPUBCHECK)`
	@mv "epubcheck-$(EPUBCHECK_VERSION)" "`dirname $(EPUBCHECK)`"
	@rm epubcheck.zip

$(KINDLEGEN):
	@echo Downloading kindlegen...
	@curl -o "kindlegen.tar.gz" -L "$(KINDLEGEN_URL)" --connect-timeout 30
	@mkdir -p `dirname $(KINDLEGEN)`
	@tar -zxf "kindlegen.tar.gz" -C `dirname $(KINDLEGEN)`
	@rm "kindlegen.tar.gz"


validate: $(SUBSETEPUB) $(EPUBCHECK)
ifndef JAVA
	@echo "Warning: Java was not found. Unable to validate ebook."
else
	@echo "Validating EPUB..."
	@$(JAVA) -jar "$(EPUBCHECK)" "$(SUBSETEPUB)"
endif


view: $(EPUBFILE)
ifndef EBOOKVIEWER
	@echo "Warning: Calibre was not found. Unable to open ebook viewer."
else
	@ebook-viewer --detach "$(EPUBFILE)"
endif


clean:
	@echo Cleaning up...
	@rm -fv "$(EPUBFILE)" "$(SUBSETEPUB)" "$(KEPUBFILE)" "$(KINDLEFILE)" "$(KINDLEFILE).epub" "$(WEIGHTEPUB)"
	rm -rf build/.book build/parts build/.parts build/latex tmpkindle tmpkepub build/fontweight
	@# only remove dir if it's empty:
	@(rmdir `dirname $(EPUBFILE)`; exit 0)


extractcurrent: $(CURRENTEPUB)
	@echo "Extracting $(CURRENTEPUB) into $(SOURCE)"
	@rm -rf "$(SOURCE)"
	@mkdir -p "$(SOURCE)"
	@unzip "$(CURRENTEPUB)" -d "$(SOURCE)"

watchcurrent: $(CURRENTEPUB) $(EPUBCHECK)
ifndef JAVA
	$(error Java was not found. Unable to validate ebook)
endif
ifndef INOTIFYWAIT
	$(error inotifywait was not found. Unable to watch ebook for changes)
endif
	@echo "Watching $(CURRENTEPUB)"
	@while true; do \
		$(INOTIFYWAIT) -qe close_write "$(CURRENTEPUB)"; \
		echo "Validating $(CURRENTEPUB)..."; \
		$(JAVA) -jar "$(EPUBCHECK)" "$(CURRENTEPUB)"; \
	done

publish: $(SUBSETEPUB) $(KINDLEFILE) $(KEPUBFILE)
	@mkdir -pv release
	cp "$(SUBSETEPUB)" "release/$$(date +$(RELEASENAME)).epub"
	cp "$(WEIGHTEPUB)" "release/$$(date +$(RELEASENAME)) Weighted.epub"
	cp "$(KEPUBFILE)" "release/$$(date +$(RELEASENAME)) Weighted Kobo.kepub.epub"
	cp "$(KINDLEFILE)" "release/$$(date +$(RELEASENAME)) Weighted Kindle.mobi"
